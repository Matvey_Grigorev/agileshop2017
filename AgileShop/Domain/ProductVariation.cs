using System.Collections.Generic;
using System.Linq;

namespace AgileShop.Domain
{
    public class ProductVariation
    {
        public ProductVariation(string name, Money price)
        {
            Name = name;
            Price = price;
            Discounts = new List<Discount>();
        }

        public string Name { get; }
        public Money Price { get; }
        private List<Discount> Discounts { get; }

        public Money GetDiscountedPrice(Quantity quantity)
        {
            var discount = GetDiscountPercent(quantity);
            return Price * (1 - discount / 100);
        }

        private decimal GetDiscountPercent(Quantity quantity)
        {
            var discount = Discounts.SingleOrDefault(_ => _.IsApplicable(quantity));
            if (discount != null) return discount.Percent;
            return 0;
        }
    }
}