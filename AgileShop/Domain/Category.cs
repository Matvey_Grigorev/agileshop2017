namespace AgileShop.Domain
{
    public enum Category
    {
        Scrum,
        Kanban,
        Toys,
        Artifacts,
        Dangerous
    }
}