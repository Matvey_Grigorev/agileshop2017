namespace AgileShop.Domain
{
    public class ColorProductVariation : ProductVariation
    {
        public ColorProductVariation(Color color, Money price) : base(color.ToString(), price)
        {
            Color = color;
        }

        public Color Color { get; }
    }
}