using System;

namespace AgileShop.Domain
{
    public class Money
    {
        public Money(decimal amount, string currencyCode = "RUB")
        {
            Amount = amount;
            CurrencyCode = currencyCode;
        }

        private decimal Amount { get; }
        private string CurrencyCode { get; }

        public static Money Zero => new Money(0, Currency.Rub);

        public static implicit operator decimal(Money money)
        {
            return money.Amount;
        }

        public static Money operator +(Money money, decimal d)
        {
            return new Money(money.Amount + d, money.CurrencyCode);
        }

        public static Money operator -(Money money, decimal d)
        {
            return new Money(money.Amount - d, money.CurrencyCode);
        }

        public static Money operator -(Money a, Money b)
        {
            if (a.CurrencyCode != b.CurrencyCode)
                throw new ArgumentException("Multicurrency operations are not supported yet. Sorry :(");

            return new Money(a.Amount - b.Amount, a.CurrencyCode);
        }

        public static Money operator *(Money money, decimal d)
        {
            return new Money(money.Amount * d, money.CurrencyCode);
        }
    }

    public static class Currency
    {
        public static string Rub = "RUB";
        public static string Usd = "USD";
    }

    public static class MoneyExtensions
    {
        public static Money Usd(this int amount)
        {
            return new Money(amount, Currency.Usd);
        }

        public static Money Usd(this double amount)
        {
            return new Money((decimal) amount, Currency.Usd);
        }
    }
}