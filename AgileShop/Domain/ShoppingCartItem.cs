﻿namespace AgileShop.Domain
{
    public class ShoppingCartItem
    {
        public ShoppingCartItem(ProductVariation productVariation)
        {
            ProductVariation = productVariation;
            Quantity = new Quantity(1);
        }

        public ProductVariation ProductVariation { get; }

        public Quantity Quantity { get; }

        public Money DiscountedPrice => ProductVariation.GetDiscountedPrice(Quantity);

        public Money SavingPerItem => ProductVariation.Price - DiscountedPrice;

        public decimal Cost => DiscountedPrice * Quantity;
    }
}